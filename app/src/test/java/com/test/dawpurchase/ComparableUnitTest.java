package com.test.dawpurchase;

import com.test.dawpurchase.api.model.ProductInfo;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ComparableUnitTest {

    /**
     * This function supports checking Comparable function for sort product Info
     */
    @Test
    public void productInfo_Comparator_ReturnsTrue() {
        ProductInfo productInfo1 = new ProductInfo();
        ProductInfo productInfo2= new ProductInfo();
        ProductInfo productInfo3 = new ProductInfo();

        List<String> userRecents1 = new ArrayList<>();
        userRecents1.add("User 1");
        productInfo1.setRecent(userRecents1);

        List<String> userRecents3 = new ArrayList<>();
        userRecents3.add("User 1");
        userRecents3.add("User 2");
        userRecents3.add("User 3");
        productInfo3.setRecent(userRecents3);

        Assert.assertTrue(productInfo1.compareTo(productInfo3) > 0);
        Assert.assertTrue(productInfo1.compareTo(productInfo2) < 0);
        Assert.assertTrue(productInfo2.compareTo(productInfo3) > 0);

    }
}