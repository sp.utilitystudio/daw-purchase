package com.test.dawpurchase;

import com.test.dawpurchase.api.invoker.ISearchInvoker;
import com.test.dawpurchase.search.ISearchUserPresenter;
import com.test.dawpurchase.search.ISearchUserView;
import com.test.dawpurchase.search.SearchUserPresenter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import io.reactivex.Observable;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SearchPresenterTest {
    @Mock
    private ISearchUserView view;
    @Mock
    private ISearchUserPresenter presenter;
    @Mock
    private ISearchInvoker invoker;

    @Before
    public void init(){
        // A convenient way to inject mocks by using the @Mock annotation in Mockito.
        //  For mock injections , initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // get the presenter reference and bind with view for testing
        presenter = new SearchUserPresenter(view, invoker);
    }


    @Test
    public void getPurchaseByUserTest() throws Exception {
        String username = "abc";
        when(invoker.searchUser(username)).thenReturn(Observable.error(new Throwable()));

        presenter.search(username);

        verify(view).onError(1, "Not found user");
    }


}
