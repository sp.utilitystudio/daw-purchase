package com.test.dawpurchase.base;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter {

    protected CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void onDestroy() {
        compositeDisposable.dispose();
    }

}
