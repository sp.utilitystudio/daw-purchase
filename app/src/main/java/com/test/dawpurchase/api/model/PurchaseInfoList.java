package com.test.dawpurchase.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PurchaseInfoList extends BaseData {
    
    @SerializedName("purchases")
    private List<PurchaseInfo> purchaseInfos;

    public List<PurchaseInfo> getPurchaseInfos() {
        return purchaseInfos;
    }

    public void setPurchaseInfos(List<PurchaseInfo> purchaseInfos) {
        this.purchaseInfos = purchaseInfos;
    }
}
