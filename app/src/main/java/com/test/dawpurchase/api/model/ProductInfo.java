package com.test.dawpurchase.api.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

public class ProductInfo implements Serializable, Comparable<ProductInfo> {

    private long id;
    private String face;
    private long price;
    private long size;
    private List<String> recent;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public List<String> getRecent() {
        return recent;
    }

    public void setRecent(List<String> recent) {
        this.recent = recent;
    }

    public int getNumberOfPurchases(){
        if(recent != null){
            return recent.size();
        }
        return 0;
    }

    @Override
    public int compareTo(ProductInfo productInfo) {
        if (getNumberOfPurchases() == productInfo.getNumberOfPurchases())
            return 0;
        else if (getNumberOfPurchases() > productInfo.getNumberOfPurchases())
            return -1;
        else
            return 1;
    }
}
