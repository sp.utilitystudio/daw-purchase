package com.test.dawpurchase.api.invoker;

import com.test.dawpurchase.api.model.ProductInfo;
import com.test.dawpurchase.api.model.PurchaseInfo;
import com.test.dawpurchase.api.model.PurchaseInfoList;
import com.test.dawpurchase.api.model.UserData;
import com.test.dawpurchase.api.rest.ProductManagerService;
import com.test.dawpurchase.api.rest.RestClient;
import com.test.dawpurchase.api.rest.UserManagerService;
import com.test.dawpurchase.util.DateTypeDeserializer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;

public class SearchInvoker implements ISearchInvoker {

    private UserManagerService userService;
    private ProductManagerService productService;

    public SearchInvoker() {
        userService = RestClient.createService(UserManagerService.class);
        productService = RestClient.createService(ProductManagerService.class);
    }

    @Override
    public Observable<UserData> searchUser(String username) {
        return userService.searchUser(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<ProductInfo>> getPurchasedProducts(String username) {
        return userService.getPurchaseByUser(username, 5)
                .map(purchaseInfoList -> purchaseInfoList.getPurchaseInfos())
                .flatMapIterable(purchaseInfos -> purchaseInfos)
                .flatMap(purchase -> mergeProductInfo(purchase.getProductId()))
                .toList()
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * It will zip two observable. One is for request product info,
     * another one is for getting number of users purchase this product
     * @param productID
     * @return
     */
    private Observable<ProductInfo> mergeProductInfo(long productID) {
        return Observable.zip(getProductInfo(productID), getUserRecentPurchase(productID), new BiFunction<ProductInfo, PurchaseInfoList, ProductInfo>() {
            @Override
            public ProductInfo apply(ProductInfo productInfo, PurchaseInfoList purchaseInfoList) throws Exception {
                if (purchaseInfoList != null && purchaseInfoList.getPurchaseInfos() != null) {
                    List<String> userPurchase = new ArrayList<>();
                    DateFormat dateFormat = new SimpleDateFormat(DateTypeDeserializer.DATE_FORMAT);
                    for (PurchaseInfo info : purchaseInfoList.getPurchaseInfos()) {
                        String date = dateFormat.format(info.getDate());
                        userPurchase.add(info.getUsername() + " : " + date);
                    }
                    productInfo.setRecent(userPurchase);
                }
                return productInfo;
            }
        });
    }

    private Observable<ProductInfo> getProductInfo(long productID) {
        return productService.getProductById(productID)
                .map(productData -> productData.getProduct())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Observable<PurchaseInfoList> getUserRecentPurchase(long productID) {
        return productService.getPurchaseByProductId(productID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
