package com.test.dawpurchase.api.model;

public class UserData extends BaseData {

    private UserInfo user;

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }
}
