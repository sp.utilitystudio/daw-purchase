package com.test.dawpurchase.api.model;

public class ProductData extends BaseData{

    private ProductInfo product;

    public ProductInfo getProduct() {
        return product;
    }

    public void setProduct(ProductInfo product) {
        this.product = product;
    }
}
