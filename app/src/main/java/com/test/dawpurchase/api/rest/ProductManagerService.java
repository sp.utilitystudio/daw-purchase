package com.test.dawpurchase.api.rest;

import com.test.dawpurchase.api.model.ProductData;
import com.test.dawpurchase.api.model.PurchaseInfoList;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ProductManagerService {

    @GET("products/{productId}")
    Observable<ProductData> getProductById(@Path("productId") long productId);

    @GET("purchases/by_product/{productId}")
    Observable<PurchaseInfoList> getPurchaseByProductId(@Path("productId") long productID, @Query("limit") int limit);

    @GET("purchases/by_product/{productId}")
    Observable<PurchaseInfoList> getPurchaseByProductId(@Path("productId") long productID);

}
