package com.test.dawpurchase.api.model;

import java.util.List;

public class ProductInfoList extends BaseData{
    private List<ProductInfo> productList;

    public List<ProductInfo> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductInfo> productList) {
        this.productList = productList;
    }


}
