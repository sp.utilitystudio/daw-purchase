package com.test.dawpurchase.api.rest;

import com.test.dawpurchase.api.model.PurchaseInfoList;
import com.test.dawpurchase.api.model.UserData;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserManagerService {

    @GET("users")
    Observable<UserData> getAllUser(@Query("limit") int limit);

    @GET("users/{username}")
    Observable<UserData> searchUser(@Path("username") String username);

    @GET("purchases/by_user/{username}")
    Observable<PurchaseInfoList> getPurchaseByUser(@Path("username") String username, @Query("limit") int limit);

    @GET("purchases/by_user/{username}")
    Observable<PurchaseInfoList> getPurchaseByUser(@Path("username") String username);

}
