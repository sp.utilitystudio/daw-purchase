package com.test.dawpurchase.api.invoker;

import com.test.dawpurchase.api.model.ProductInfo;
import com.test.dawpurchase.api.model.PurchaseInfo;
import com.test.dawpurchase.api.model.UserData;

import java.util.List;

import io.reactivex.Observable;

public interface ISearchInvoker {

    Observable<UserData> searchUser(String username);

    Observable<List<ProductInfo>> getPurchasedProducts(String username);

}
