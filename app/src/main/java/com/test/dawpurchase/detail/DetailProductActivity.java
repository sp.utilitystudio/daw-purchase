package com.test.dawpurchase.detail;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.test.dawpurchase.R;
import com.test.dawpurchase.api.model.ProductInfo;
import com.test.dawpurchase.databinding.ActivityDetailProductBinding;

public class DetailProductActivity extends AppCompatActivity {

    ActivityDetailProductBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_product);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        ProductInfo productInfo = (ProductInfo) getIntent().getExtras().getSerializable("product");
        if (productInfo != null) {
            if (actionBar != null) {
                actionBar.setTitle("Product #" + productInfo.getId());
            }
            binding.txtNo.setText("Product Id: " + productInfo.getId());
            binding.txtFace.setText("Product Face: " + productInfo.getFace());
            binding.txtPrice.setText("Product Price: " + productInfo.getPrice() + " $");
            binding.txtSize.setText("Product Size: " + productInfo.getSize());

            if(productInfo.getNumberOfPurchases() > 0){
                StringBuilder result = new StringBuilder();
                result.append("Product sales: " + productInfo.getNumberOfPurchases() + "\n");
                for(String user: productInfo.getRecent()){
                    result.append("-" + user);
                    result.append("\n");
                }
                binding.txtPurchased.setText(result.toString());
            }else{
                binding.txtPurchased.setText("Product no sales");
            }

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
