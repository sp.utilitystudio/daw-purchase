package com.test.dawpurchase.search;

public interface IPresenter {

    void onError(int errorCode, String message);

    void onDestroy();

}
