package com.test.dawpurchase.search;

public interface ISearchUserPresenter extends IPresenter{

    void search(String username);

    void getPurchasesByUser(String username);

}
