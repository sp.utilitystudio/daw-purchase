package com.test.dawpurchase.search;

import com.google.gson.Gson;
import com.test.dawpurchase.api.model.ProductInfoList;
import com.test.dawpurchase.util.LogDaw;
import com.test.dawpurchase.base.BasePresenter;
import com.test.dawpurchase.api.invoker.ISearchInvoker;

public class SearchUserPresenter extends BasePresenter implements ISearchUserPresenter {

    private ISearchUserView view;
    private ISearchInvoker invoker;

    public SearchUserPresenter(ISearchUserView view, ISearchInvoker invoker) {
        this.view = view;
        this.invoker = invoker;
    }

    @Override
    public void search(String username) {
        compositeDisposable.add(invoker.searchUser(username).subscribe(userData -> {
            LogDaw.i("Data response " + new Gson().toJson(userData));
            if(view != null){
                view.onSuccess(userData);
            }
        }, throwable -> onError(1, "Not found user")));
    }

    @Override
    public void getPurchasesByUser(String username) {
        compositeDisposable.add(invoker.getPurchasedProducts(username).subscribe(productInfos -> {
            LogDaw.i("Purchase response " + new Gson().toJson(productInfos));
            ProductInfoList productInfoList = new ProductInfoList();
            productInfoList.setProductList(productInfos);
            if(view != null){
                view.onSuccess(productInfoList);
            }
        }, throwable ->  onError(2, "Not found purchases")));
    }

    @Override
    public void onError(int errorCode, String message) {
        if(view != null){
            view.onError(errorCode, message);
        }
    }
}
