package com.test.dawpurchase.search;

import com.test.dawpurchase.api.model.BaseData;

public interface ISearchUserView {

    void onSuccess(BaseData data);

    void onError(int errorCode, String message);

}
