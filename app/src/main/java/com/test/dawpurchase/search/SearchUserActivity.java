package com.test.dawpurchase.search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.test.dawpurchase.R;
import com.test.dawpurchase.adapter.PurchaseAdapter;
import com.test.dawpurchase.api.invoker.SearchInvoker;
import com.test.dawpurchase.api.model.BaseData;
import com.test.dawpurchase.api.model.ProductInfo;
import com.test.dawpurchase.api.model.ProductInfoList;
import com.test.dawpurchase.api.model.UserData;
import com.test.dawpurchase.api.model.UserInfo;
import com.test.dawpurchase.databinding.ActivitySearchUserBinding;
import com.test.dawpurchase.detail.DetailProductActivity;

import java.util.ArrayList;

public class SearchUserActivity extends AppCompatActivity implements ISearchUserView, PurchaseAdapter.OnItemClickedListener {

    ISearchUserPresenter presenter;
    ActivitySearchUserBinding binding;
    PurchaseAdapter purchaseAdapter;

    String NOT_FOUND_STR_FORMAT = "User with username of %s was not found";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_user);

        presenter = new SearchUserPresenter(this, new SearchInvoker());
        purchaseAdapter = new PurchaseAdapter(this, new ArrayList<>());
        purchaseAdapter.setOnItemClickListener(this);

        binding.rycResult.setItemAnimator(new DefaultItemAnimator());
        binding.rycResult.setLayoutManager(new LinearLayoutManager(this));
        binding.rycResult.setAdapter(purchaseAdapter);
    }

    public void searchClicked(View view) {
        String text = binding.editUsername.getText().toString();
        if(!TextUtils.isEmpty(text)){
            purchaseAdapter.clearData();
            presenter.search(text);
        }
    }

    @Override
    public void onSuccess(BaseData data) {
        if(data instanceof UserData){
            processUserData((UserData) data);
        } else if (data instanceof ProductInfoList) {
            processPurchaseList((ProductInfoList) data);
        }
    }

    @Override
    public void onError(int errorCode, String message) {
        switch (errorCode) {
            case 1:
                Toast.makeText(this, "Not found user", Toast.LENGTH_LONG).show();
                break;
            case 2:
                Toast.makeText(this, "Not found product purchase", Toast.LENGTH_LONG).show();
                break;
            default:
                Toast.makeText(this, "Something wrongs. Please try again", Toast.LENGTH_LONG).show();
                break;
        }

    }

    public void processUserData(UserData userData) {
        if(userData != null && userData.getUser() != null
                && TextUtils.isEmpty(userData.getUser().getUsername()) == false){
            UserInfo userInfo = userData.getUser();
            binding.txtResult.setText("User: " + userInfo.getUsername());
            presenter.getPurchasesByUser(userInfo.getUsername());
        }else{
            binding.txtResult.setText(String.format(NOT_FOUND_STR_FORMAT, binding.editUsername.getText()));
            binding.txtNotFound.setVisibility(View.VISIBLE);
        }
    }

    public void processPurchaseList(ProductInfoList purchaseInfoList) {
        if(purchaseInfoList != null
                && purchaseInfoList.getProductList() != null && purchaseInfoList.getProductList().size() > 0){
            purchaseAdapter.updateData(purchaseInfoList.getProductList());
            binding.txtNotFound.setVisibility(View.GONE);
        }else{
            binding.txtNotFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemClicked(int position) {
        ProductInfo productInfo = purchaseAdapter.getItem(position);

        Bundle bundle = new Bundle();
        bundle.putSerializable("product", productInfo);
        Intent intent = new Intent(SearchUserActivity.this, DetailProductActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

}
