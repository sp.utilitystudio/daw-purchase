package com.test.dawpurchase.util;

import android.util.Log;

import com.google.gson.Gson;

public class LogDaw {

    public static final String TAG = "LogDaw";

    public static void d(String message) {
        StackTraceElement[] ste = (new Throwable()).getStackTrace();
        String text = "[" + ste[1].getFileName() + ":" + ste[1].getLineNumber() + ":" + ste[1].getMethodName() + "()]";
        Log.d(TAG, text + message);
    }

    public static void i(String message) {
        StackTraceElement[] ste = (new Throwable()).getStackTrace();
        String text = "[" + ste[1].getFileName() + ":" + ste[1].getLineNumber() + ":" + ste[1].getMethodName() + "()]";
        Log.i(TAG, text + message);
    }

    public static void e(String message) {
        StackTraceElement[] ste = (new Throwable()).getStackTrace();
        String text = "[" + ste[1].getFileName() + ":" + ste[1].getLineNumber() + ":" + ste[1].getMethodName() + "()]";
        Log.e(TAG, text + message);
    }

}
