package com.test.dawpurchase.util;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateTypeDeserializer implements JsonDeserializer<Date>, JsonSerializer<Date> {

    private static final String TAG = DateTypeDeserializer.class.getName();
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";//2016-11-07T07:41:39Z 2019-08-17T22:36:31.819Z

    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            if (json.getAsString() == null || json.getAsString().compareTo("") == 0) {
                return new Date(170985600);//1979 to seconds
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return simpleDateFormat.parse(json.getAsString());
        } catch (Exception e) {
            Log.d(TAG, "Error when deserialize date " + e.getMessage() + " using locale English ");
        }
        throw new JsonParseException("Cant parse date: \"" + json.getAsString()
                + "\". Supported formats: \n" + DATE_FORMAT + " with Locales: " + Locale.ENGLISH);
    }

    @Override
    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            String dateTimeString = simpleDateFormat.format(src);
            return context.serialize(dateTimeString);
        } catch (Exception e) {
            Log.d(TAG, "Error when serialize date " + e.getMessage() + " using locale " + Locale.ENGLISH);
        }
        return context.serialize("");
    }

}
