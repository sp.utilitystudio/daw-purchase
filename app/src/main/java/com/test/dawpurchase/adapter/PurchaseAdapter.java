package com.test.dawpurchase.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.dawpurchase.databinding.ItemPurchaseBinding;
import com.test.dawpurchase.api.model.ProductInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PurchaseAdapter extends RecyclerView.Adapter<PurchaseAdapter.ViewHolder> {

    private List<ProductInfo> data = new ArrayList<>();
    private Context context;
    private OnItemClickedListener listener;

    public void clearData() {
        this.data.clear();
        notifyDataSetChanged();
    }

    public interface OnItemClickedListener{
        void onItemClicked(int position);
    }

    public PurchaseAdapter(Context context, List<ProductInfo> data){
        this.context = context;
        this.data = data;
    }

    public void setOnItemClickListener(OnItemClickedListener listener){
        this.listener = listener;
    }

    public void updateData(List<ProductInfo> data){
        this.data.clear();

        Collections.sort(data);
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public ProductInfo getItem(int position){
        return data.get(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        ItemPurchaseBinding binding = ItemPurchaseBinding.inflate(LayoutInflater.from(context), viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.binding.txtNo.setText(data.get(position).getId() + "");
        viewHolder.binding.txtFace.setText(data.get(position).getFace() + "");
        viewHolder.binding.txtPrice.setText(data.get(position).getPrice() + " $");
        if(data.get(position).getRecent() != null){
            viewHolder.binding.txtPurchased.setText(data.get(position).getRecent().size() + " sales");
        }else{
            viewHolder.binding.txtPurchased.setText("N/A");
        }
        viewHolder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null){
                    listener.onItemClicked(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemPurchaseBinding binding;
        public ViewHolder(ItemPurchaseBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

}
