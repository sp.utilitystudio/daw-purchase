package com.test.dawpurchase;

import android.app.Application;

import com.test.dawpurchase.api.rest.RestClient;

public class DawApplication extends Application {

    private static final String BASE_URL = "http://192.168.99.122:8000/api/";

    @Override
    public void onCreate() {
        super.onCreate();
        RestClient.init(BASE_URL);
    }

}
