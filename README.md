# daw-purchase
1. The git repo: https://gitlab.com/sp.utilitystudio/daw-purchase
2. The summary information:
- We designed the daw-purchase app follow MVP model. The 3rd library for Rest APIs and handle data: Retrofit, RxJava2
- User input name, the app will show top 5 recent products that purchased by user.
- Click any one product, it will navigate to DetailProduct screen
- Write down 1 unit test and 1 integration test using Mockito

